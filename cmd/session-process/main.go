// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"go.uber.org/zap"

	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	schemeProcessRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
	sessionServiceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/internal/http_infra"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/events"
)

type options struct {
	ListenAddress            string `long:"listen-address" env:"LISTEN_ADDRESS" default:"0.0.0.0:80" description:"Address for the session-process api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	SchemeDiscoveryUrl       string `long:"scheme-discovery-url" env:"SCHEME_DISCOVERY_URL" default:"http://localhost:80" description:"Scheme discovery url."`
	SessionServiceAddress    string `long:"session-service-address" env:"SESSION_SERVICE_ADDRESS" default:"http://localhost:80" description:"Session service address."`
	SessionServiceAPIKey     string `long:"session-service-api-key" env:"SESSION_SERVICE_API_KEY" default:"" description:"API key to use when calling the session service."`
	BkConfigServiceAddress   string `long:"bk-config-service-address" env:"BK_CONFIG_SERVICE_ADDRESS" default:"http://localhost:80" description:"Bk Config service address."`
	BkConfigServiceAPIKey    string `long:"bk-config-service-api-key" env:"BK_CONFIG_SERVICE_API_KEY" default:"" description:"API key to use when calling the bk-config service."`
	SessionExpirationMinutes string `long:"session-expiration-minutes" env:"SESSION_EXPIRATION_MINUTES" default:"2" description:"Max length of a session in minutes."`
	LogOptions
}

func main() {
	cliOptions, err := parseOptions()
	if err != nil {
		log.Fatalf("failed to parse options: %v", err)
	}

	logger, err := newLogger(cliOptions.LogOptions)
	if err != nil {
		log.Fatalf("failed to create logger: %v", err)
	}

	sessionServiceRepository := sessionServiceRepositories.NewSessionClient(cliOptions.SessionServiceAddress, cliOptions.SessionServiceAPIKey)

	bkConfigRepository := bkConfigRepositories.NewBkConfigClient(cliOptions.BkConfigServiceAddress, cliOptions.BkConfigServiceAPIKey)

	schemeProcessRepository := schemeProcessRepositories.NewSchemeClient(cliOptions.SchemeDiscoveryUrl)

	sessionExpirationMinutes, err := strconv.Atoi(cliOptions.SessionExpirationMinutes)
	if err != nil {
		event := events.SEP_37
		logger.Log(zap.FatalLevel, event.Message, zap.Error(err), zap.Reflect("event", event))
	}

	sessionUseCase := usecases.NewSessionUseCase(
		logger,
		sessionServiceRepository,
		bkConfigRepository,
		schemeProcessRepository,
		sessionExpirationMinutes,
	)

	router := http_infra.NewRouter(sessionUseCase, logger)

	event := events.SEP_1
	logger.Log(event.GetLogLevel(), event.Message, zap.String("listenAddress", cliOptions.ListenAddress), zap.Reflect("event", event))
	err = http.ListenAndServe(cliOptions.ListenAddress, router)
	if err != nil {
		event = events.SEP_2
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		if !errors.Is(err, http.ErrServerClosed) {
			panic(err)
		}
	}
}

func newLogger(logOptions LogOptions) (*zap.Logger, error) {
	config := logOptions.ZapConfig()
	logger, err := config.Build()
	if err != nil {
		return nil, fmt.Errorf("failed to create new zap logger: %v", err)
	}

	return logger, nil
}
