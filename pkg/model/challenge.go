// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import "gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

type Challenge struct {
	Nonce                            string              `json:"nonce"`
	OrganizationNonceSignature       base64.Base64String `json:"organizationNonceSignature"`
	EphemeralOrganizationEcPublicKey string              `json:"ephemeralOrganizationEcPublicKey"`
}
