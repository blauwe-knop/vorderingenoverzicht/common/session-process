// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import "gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

type ChallengeResponse struct {
	Nonce                    string              `json:"nonce"`
	EncryptedChallengeResult base64.Base64String `json:"encryptedChallengeResult"`
}
