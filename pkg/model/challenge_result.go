// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package model

import "gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

type ChallengeResult struct {
	AppNonceSignature base64.Base64String `json:"appNonceSignature"`
	AppPublicKey      string              `json:"appPublicKey"`
	CertificateType   string              `json:"certificateType"`
	Certificate       string              `json:"certificate,omitempty"`
	SessionAesKey     base64.Base64String `json:"sessionAesKey"`
}
