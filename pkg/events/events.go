package events

var (
	SEP_1 = NewEvent(
		"sep_1",
		"started listening",
		Low,
	)
	SEP_2 = NewEvent(
		"sep_2",
		"server closed",
		High,
	)
	SEP_3 = NewEvent(
		"sep_3",
		"received request for openapi spec as JSON",
		Low,
	)
	SEP_4 = NewEvent(
		"sep_4",
		"sent response with openapi spec as JSON",
		Low,
	)
	SEP_5 = NewEvent(
		"sep_5",
		"received request for openapi spec as YAML",
		Low,
	)
	SEP_6 = NewEvent(
		"sep_6",
		"sent response with openapi spec as YAML",
		Low,
	)
	SEP_8 = NewEvent(
		"sep_8",
		"failed to read openapi.json file",
		High,
	)
	SEP_9 = NewEvent(
		"sep_9",
		"failed to write fileBytes to response",
		High,
	)
	SEP_10 = NewEvent(
		"sep_10",
		"failed to read openapi.yaml file",
		High,
	)
	SEP_14 = NewEvent(
		"sep_14",
		"received request for challenge",
		Low,
	)
	SEP_15 = NewEvent(
		"sep_15",
		"sent response with signed challenge",
		Low,
	)
	SEP_16 = NewEvent(
		"sep_16",
		"received request to complete challenge",
		Low,
	)
	SEP_17 = NewEvent(
		"sep_17",
		"sent response with new session token",
		Low,
	)
	SEP_18 = NewEvent(
		"sep_18",
		"failed to decode request payload",
		High,
	)
	SEP_19 = NewEvent(
		"sep_19",
		"failed to create challenge",
		High,
	)
	SEP_20 = NewEvent(
		"sep_20",
		"failed to encode response payload",
		High,
	)
	SEP_21 = NewEvent(
		"sep_21",
		"failed to create session",
		High,
	)
	SEP_22 = NewEvent(
		"sep_22",
		"failed to retrieve organization root private key",
		High,
	)
	SEP_23 = NewEvent(
		"sep_23",
		"failed to decode private key pem",
		High,
	)
	SEP_24 = NewEvent(
		"sep_24",
		"failed to decode ciphertext from request",
		High,
	)
	SEP_25 = NewEvent(
		"sep_25",
		"failed to decrypt challenge",
		High,
	)
	SEP_26 = NewEvent(
		"sep_26",
		"failed to unmarshal challenge request message",
		High,
	)
	SEP_27 = NewEvent(
		"sep_27",
		"failed to create nonce",
		High,
	)
	SEP_28 = NewEvent(
		"sep_28",
		"failed to sign message",
		High,
	)
	SEP_29 = NewEvent(
		"sep_29",
		"nonce does not exist",
		High,
	)
	SEP_30 = NewEvent(
		"sep_30",
		"failed to delete nonce",
		High,
	)
	SEP_31 = NewEvent(
		"sep_31",
		"challenge expired",
		High,
	)
	SEP_32 = NewEvent(
		"sep_32",
		"failed to parse app public key from pem",
		High,
	)
	SEP_33 = NewEvent(
		"sep_33",
		"failed to verify app signature",
		High,
	)
	SEP_34 = NewEvent(
		"sep_34",
		"invalid signature",
		High,
	)
	SEP_35 = NewEvent(
		"sep_35",
		"failed to fetch organization root key pairs list",
		High,
	)
	SEP_36 = NewEvent(
		"sep_36",
		"key pair does not exist",
		High,
	)
	SEP_37 = NewEvent(
		"sep_37",
		"failed to parse sessionExpirationMinutes to int",
		VeryHigh,
	)
	SEP_38 = NewEvent(
		"sep_38",
		"failed to generate ephemeral key-pair",
		High,
	)
	SEP_39 = NewEvent(
		"sep_39",
		"failed to convert organization key-pair to PEM",
		High,
	)
	SEP_40 = NewEvent(
		"sep_40",
		"failed to parse token unverified",
		High,
	)
	SEP_41 = NewEvent(
		"sep_41",
		"organization private key invalid",
		High,
	)
	SEP_42 = NewEvent(
		"sep_42",
		"app public key invalid",
		High,
	)
	SEP_43 = NewEvent(
		"sep_43",
		"aes key invalid",
		High,
	)
	SEP_44 = NewEvent(
		"sep_44",
		"certificate root app ec public key not equal to app public key pem",
		High,
	)
	SEP_45 = NewEvent(
		"sep_45",
		"failed to fetch app manager",
		High,
	)
	SEP_46 = NewEvent(
		"sep_46",
		"error parsing app manager public key",
		High,
	)
	SEP_47 = NewEvent(
		"sep_47",
		"invalid app manager ec public key",
		High,
	)
	SEP_48 = NewEvent(
		"sep_48",
		"certificate with scope is unsupported",
		High,
	)
	SEP_49 = NewEvent(
		"sep_49",
		"unexpected JWT signing method",
		High,
	)
	SEP_50 = NewEvent(
		"sep_50",
		"error certificate expired",
		High,
	)
	SEP_51 = NewEvent(
		"sep_51",
		"failed to verify and parse token",
		High,
	)
	SEP_52 = NewEvent(
		"sep_52",
		"error invalid request",
		High,
	)
	SEP_53 = NewEvent(
		"sep_53",
		"unknown certificate type",
		High,
	)
	SEP_54 = NewEvent(
		"sep_54",
		"failed to ephemeral organization public key to pem",
		High,
	)
	SEP_55 = NewEvent(
		"sep_55",
		"failed to base64 encode sessionAesKey",
		High,
	)
	SEP_56 = NewEvent(
		"sep_56",
		"failed to base64 encode AppNonceSignature",
		High,
	)
	SEP_57 = NewEvent(
		"sep_57",
		"failed to parse created session aes key to bytes",
		High,
	)
	SEP_58 = NewEvent(
		"sep_58",
		"failed to encrypt session token",
		High,
	)
)
