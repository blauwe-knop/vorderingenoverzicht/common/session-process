// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
)

type SessionClient struct {
	baseURL string
}

var ErrSessionNotFound = errors.New("session does not exist")

func NewSessionClient(baseURL string) *SessionClient {
	return &SessionClient{
		baseURL: baseURL,
	}
}

func (s *SessionClient) RequestChallenge() (*model.Challenge, error) {
	url := fmt.Sprintf("%s/challenge", s.baseURL)

	request, err := http.NewRequest(http.MethodPost, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to post RequestChallenge request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post RequestChallenge: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving RequestChallenge: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	challenge := model.Challenge{}
	err = json.Unmarshal(body, &challenge)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, Challenge: %v", err)
	}

	return &challenge, nil
}

func (s *SessionClient) CompleteChallenge(challengeResponse model.ChallengeResponse) (base64.Base64String, error) {
	url := fmt.Sprintf("%s/challenge/complete", s.baseURL)

	requestBodyAsJson, err := json.Marshal(challengeResponse)
	if err != nil {
		return "", fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return "", fmt.Errorf("failed to post CompleteChallenge request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return "", fmt.Errorf("failed to post CompleteChallenge: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("unexpected status while retrieving CompleteChallenge: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %v", err)
	}

	var encryptedSessionToken base64.Base64String
	err = json.Unmarshal(body, &encryptedSessionToken)
	if err != nil {
		return "", fmt.Errorf("failed to decode json, session: %v", err)
	}

	return encryptedSessionToken, nil
}

func (s *SessionClient) GetHealth() error {
	url := fmt.Sprintf("%s/health", s.baseURL)
	resp, err := http.Get(url)
	if err != nil {
		return fmt.Errorf("failed to fetch session process health: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to fetch session process health with status: %d", resp.StatusCode)
	}

	return nil
}

func (s *SessionClient) GetHealthCheck() healthcheck.Result {
	name := "session-process"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
