// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
)

type SessionServiceRepository interface {
	RequestChallenge() (*model.Challenge, error)
	CompleteChallenge(challengeResponse model.ChallengeResponse) (base64.Base64String, error)
	GetHealth() error
	healthcheck.Checker
}
