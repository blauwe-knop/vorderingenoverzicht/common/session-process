#!/bin/bash

mockgen -destination=./../test/mock/session_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories SessionRepository
mockgen -destination=./../test/mock/bk_config_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories BkConfigRepository
mockgen -destination=./../test/mock/scheme_process_repository.go -package=mock gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories SchemeRepository
