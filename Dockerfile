FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/session-process/api
COPY ./cmd /go/src/session-process/cmd
COPY ./internal /go/src/session-process/internal
COPY ./pkg /go/src/session-process/pkg
COPY ./go.mod /go/src/session-process/
COPY ./go.sum /go/src/session-process/
WORKDIR /go/src/session-process
RUN go mod download \
 && go build -o dist/bin/session-process ./cmd/session-process

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/session-process/dist/bin/session-process /usr/local/bin/session-process
COPY --from=build /go/src/session-process/api/openapi.json /api/openapi.json
COPY --from=build /go/src/session-process/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/session-process"]
