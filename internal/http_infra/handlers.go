// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"net/http"

	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
)

func handlerRequestChallenge(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	sessionUseCase, _ := context.Value(sessionUseCaseKey).(*usecases.SessionUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SEP_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	signedChallenge, err := sessionUseCase.RequestChallenge()

	if err != nil {
		event := events.SEP_19
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(signedChallenge)
	if err != nil {
		event := events.SEP_20
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SEP_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCompleteChallenge(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	sessionUseCase, _ := context.Value(sessionUseCaseKey).(*usecases.SessionUseCase)
	logger, _ := context.Value(loggerKey).(*zap.Logger)

	event := events.SEP_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var challengeResponse model.ChallengeResponse
	err := json.NewDecoder(request.Body).Decode(&challengeResponse)
	if err != nil {
		event := events.SEP_18
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	sessionToken, err := sessionUseCase.CompleteChallenge(challengeResponse)

	if err != nil {
		event := events.SEP_21
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(sessionToken)
	if err != nil {
		event := events.SEP_19
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SEP_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
