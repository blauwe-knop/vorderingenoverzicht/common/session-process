package internal

import (
	"github.com/hashicorp/go-retryablehttp"
)

var _ = retryablehttp.Client{}
