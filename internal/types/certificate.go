package types

type CertificateType string

const (
	CertificateTypeNone      CertificateType = "CertificateTypeNone"
	AppManagerJWTCertificate CertificateType = "AppManagerJWTCertificate"
	CertificateTypeUnknown   CertificateType = "CertificateTypeUnknown"
)

func CertificateTypeFromValue(value string) CertificateType {
	switch value {
	case "CertificateTypeNone":
		return CertificateTypeNone
	case "AppManagerJWTCertificate":
		return AppManagerJWTCertificate
	default:
		return CertificateTypeUnknown
	}
}
