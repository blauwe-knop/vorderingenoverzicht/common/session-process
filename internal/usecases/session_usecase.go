// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package usecases

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/connect/go-connect/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecies"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"

	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	serviceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	schemeProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
	schemeProcessRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/internal/types"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
)

type SessionUseCase struct {
	Logger                   *zap.Logger
	SessionServiceRepository repositories.SessionRepository
	BkConfigRepository       bkConfigRepositories.BkConfigRepository
	SchemeProcessRepository  schemeProcessRepositories.SchemeRepository
	SessionExpirationMinutes int
}

var ErrKeyPairDoesNotExist = errors.New("error key pair does not exist")
var ErrInvalidRequest = errors.New("error invalid request")
var ErrCertificateExpired = errors.New("error certificate expired")
var ErrUnknownCertificateType = errors.New("error unknown certificate type")

func NewSessionUseCase(logger *zap.Logger, sessionServiceRepository repositories.SessionRepository, bkConfigRepository bkConfigRepositories.BkConfigRepository, schemeProcessRepository schemeProcessRepositories.SchemeRepository, sessionExpirationMinutes int) *SessionUseCase {
	return &SessionUseCase{
		Logger:                   logger,
		SessionServiceRepository: sessionServiceRepository,
		BkConfigRepository:       bkConfigRepository,
		SchemeProcessRepository:  schemeProcessRepository,
		SessionExpirationMinutes: sessionExpirationMinutes,
	}
}

func (uc *SessionUseCase) RequestChallenge() (*model.Challenge, error) {
	organizationRootPrivateKey, err := uc.getOrganizationRootPrivateKey()
	if err != nil {
		event := events.SEP_22
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to retrieve organizaton root private key: %v", err)
	}

	ecOrganizationRootPrivateKey, err := ec.ParsePrivateKeyFromPem([]byte(organizationRootPrivateKey))
	if err != nil {
		event := events.SEP_23
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to decode private key pem: %v", err)
	}

	if !ecOrganizationRootPrivateKey.Verify() {
		event := events.SEP_41
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return nil, fmt.Errorf("ec organization root private key invalid")
	}

	ephemeralOrganizationKeypair, err := ec.GenerateKeyPair(ecOrganizationRootPrivateKey.Curve)
	if err != nil {
		event := events.SEP_38
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to generate ephemeral key-pair: %v", err)
	}

	ephemeralOrganizationKeyPairPem, err := ephemeralOrganizationKeypair.ToPem()
	if err != nil {
		event := events.SEP_39
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to convert organization key-pair to PEM: %v", err)
	}

	nonce := serviceModel.Nonce{
		Nonce:                        uuid.NewString(),
		EphemeralOrganizationKeyPair: string(ephemeralOrganizationKeyPairPem),
		CreatedAt:                    serviceModel.JSONTime(time.Now()),
	}

	createdNonce, err := uc.SessionServiceRepository.CreateNonce(nonce)
	if err != nil {
		event := events.SEP_27
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to create nonce: %v", err)
	}

	signature, err := ecdsa.Sign(ecOrganizationRootPrivateKey, []byte(createdNonce.Nonce))
	if err != nil {
		event := events.SEP_28
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to sign message: %v", err)
	}

	ephemeralOrganizationPublicKey, err := ephemeralOrganizationKeypair.PublicKey.ToPem()
	if err != nil {
		event := events.SEP_54
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return nil, fmt.Errorf("failed to ephemeral organization public key to pem: %v", err)
	}

	return &model.Challenge{
		Nonce:                            createdNonce.Nonce,
		OrganizationNonceSignature:       base64.ParseFromBytes(signature),
		EphemeralOrganizationEcPublicKey: string(ephemeralOrganizationPublicKey),
	}, nil
}

func (uc *SessionUseCase) CompleteChallenge(challengeResponse model.ChallengeResponse) (base64.Base64String, error) {
	nonce, err := uc.SessionServiceRepository.DeleteNonce(challengeResponse.Nonce)
	if nonce == nil {
		event := events.SEP_29
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("nonce does not exist: %v", err)
	}
	if err != nil {
		event := events.SEP_30
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to delete nonce: %v", err)
	}

	ecPrivateKey, err := ec.ParsePrivateKeyFromPem([]byte(nonce.EphemeralOrganizationKeyPair))
	if err != nil {
		event := events.SEP_23
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode private key pem: %v", err)
	}

	ciphertext, err := base64.Base64String(challengeResponse.EncryptedChallengeResult).ToBytes()
	if err != nil {
		event := events.SEP_24
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decode ciphertext from request: %v", err)
	}

	decryptedChallengeResult, err := ecies.Decrypt(ecPrivateKey, ciphertext)
	if err != nil {
		event := events.SEP_25
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to decrypt challenge: %v", err)
	}

	var challengeResult model.ChallengeResult
	err = json.Unmarshal(decryptedChallengeResult, &challengeResult)
	if err != nil {
		event := events.SEP_26
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to unmarshal challenge request message: %v", err)
	}

	createdAtNonce := (time.Time)(nonce.CreatedAt)
	now := time.Now()
	difference := createdAtNonce.Sub(now)
	if difference.Minutes() > 1 {
		event := events.SEP_31
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("challenge expired: %v", err)
	}

	ecAppPublicKey, err := ec.ParsePublicKeyFromPem([]byte(challengeResult.AppPublicKey))
	if err != nil {
		event := events.SEP_32
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to parse app public key from pem: %v", err)
	}

	if !ecAppPublicKey.Verify() {
		event := events.SEP_42
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("app public key invalid")
	}

	sessionAesKey, err := base64.Base64String(challengeResult.SessionAesKey).ToBytes()
	if err != nil {
		event := events.SEP_55
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to base64 encode sessionAesKey: %v", err)
	}

	if !aes.VerifyKey(sessionAesKey) {
		event := events.SEP_43
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
		return "", fmt.Errorf("session aes key invalid")
	}

	appNonceSignatureBytes, err := base64.Base64String(challengeResult.AppNonceSignature).ToBytes()
	if err != nil {
		event := events.SEP_56
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to base64 encode AppNonceSignature: %v", err)
	}

	verified := ecdsa.Verify(ecAppPublicKey, []byte(challengeResponse.Nonce), appNonceSignatureBytes)
	if !verified {
		event := events.SEP_33
		uc.Logger.Log(event.GetLogLevel(),
			event.Message,
			zap.Error(err),
			zap.Reflect("event", event))
		return "", fmt.Errorf("failed to verify app signature")
	}

	var scope string
	var bsn string

	certificateType := types.CertificateTypeFromValue(challengeResult.CertificateType)
	if certificateType == types.CertificateTypeNone {
		scope = ""
		bsn = ""
	} else if certificateType == types.AppManagerJWTCertificate {
		parser := jwt.NewParser()
		unverifiedCertificateToken, _, err := parser.ParseUnverified(challengeResult.Certificate, jwt.MapClaims{})
		if err != nil {
			event := events.SEP_40
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return "", fmt.Errorf("failed to parse token unverified: %v", err)
		}

		scope = unverifiedCertificateToken.Claims.(jwt.MapClaims)["scope"].(string)
		appManagerOin := unverifiedCertificateToken.Claims.(jwt.MapClaims)["app_manager_oin"].(string)
		//TODO: https://gitlab.com/blauwe-knop/vorderingenoverzicht/issues/-/issues/650
		//keyId := unverifiedCertificateToken.Claims.(jwt.MapClaims)["key_id"].(string)
		certificateRootAppEcPublicKeyPem := unverifiedCertificateToken.Claims.(jwt.MapClaims)["app_public_key"].(string)

		var appManager *schemeProcessModel.AppManager

		if certificateRootAppEcPublicKeyPem != challengeResult.AppPublicKey {
			event := events.SEP_44
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
			return "", fmt.Errorf("certificate root app ec public key not equal to app public key")
		}

		if scope == "nl.vorijk.oauth_scope.blauwe_knop" {
			//TODO: https://gitlab.com/blauwe-knop/vorderingenoverzicht/issues/-/issues/650
			//TODO: 39 - lookup scheme manager root EC public key and scheme url
			//TODO: 40 - verify scheme manager EC public key size and type
			//TODO: 41-43 - fetch (signed) scheme
			//TODO: 44 - verify scheme manager signature on scheme document using scheme manager root EC public key
			//TODO: 45 - lookup app manager EC public key corresponding to AppManagerOin and keyId
			appManager, err = uc.SchemeProcessRepository.FetchAppManager(appManagerOin)
			if err != nil {
				event := events.SEP_45
				uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
				return "", fmt.Errorf("failed to fetch app manager: %v", err)
			}
		} else {
			event := events.SEP_48
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return "", fmt.Errorf("certificate with scope %s is unsupported", scope)
		}

		appManagerEcPublicKey, err := ec.ParsePublicKeyFromPem([]byte(appManager.PublicKey))
		if err != nil {
			event := events.SEP_46
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
			return "", fmt.Errorf("error parsing app manager public key")
		}

		if !appManagerEcPublicKey.Verify() {
			event := events.SEP_47
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
			return "", fmt.Errorf("invalid app manager ec public key")
		}

		appManagerJwtCertificate, err := jwt.Parse(challengeResult.Certificate, func(jwtToken *jwt.Token) (interface{}, error) {
			if _, ok := jwtToken.Method.(*jwt.SigningMethodECDSA); !ok {
				event := events.SEP_49
				errorMessage := fmt.Sprintf("unexpected JWT signing method: %s", jwtToken.Header["alg"])
				uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("jwtSigningMethod", errorMessage), zap.Reflect("event", event))
				return nil, fmt.Errorf(errorMessage)
			}
			return appManagerEcPublicKey.ToEcdsa(), nil
		})
		if err != nil {
			if strings.Contains(err.Error(), jwt.ErrTokenExpired.Error()) {
				event := events.SEP_50
				uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
				return "", ErrCertificateExpired
			}

			event := events.SEP_51
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
			return "", fmt.Errorf("failed to verify and parse token: %v", err)
		}

		claims, ok := appManagerJwtCertificate.Claims.(jwt.MapClaims)
		if !ok {
			event := events.SEP_52
			uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
			return "", ErrInvalidRequest
		}

		bsn = claims["bsn"].(string)
	} else {
		event := events.SEP_53
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.String("certificateType", string(certificateType)), zap.Reflect("event", event))
		return "", ErrUnknownCertificateType
	}

	createdAt := time.Now()
	expiresAt := createdAt.Add(time.Minute * time.Duration(uc.SessionExpirationMinutes))
	session := &serviceModel.Session{
		Token:        uuid.NewString(),
		AppPublicKey: challengeResult.AppPublicKey,
		AesKey:       challengeResult.SessionAesKey,
		Scope:        scope,
		Bsn:          bsn,
		CreatedAt:    serviceModel.JSONTime(createdAt),
		ExpiresAt:    serviceModel.JSONTime(expiresAt),
	}

	createdSession, err := uc.SessionServiceRepository.CreateSession(*session)
	if err != nil {
		event := events.SEP_21
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to create session: %v", err)
	}

	createdSessionAesKey, err := base64.Base64String(createdSession.AesKey).ToBytes()
	if err != nil {
		event := events.SEP_57
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to parse created session aes key to bytes: %v", err)
	}

	encryptedSessionToken, err := aes.Encrypt(createdSessionAesKey, []byte(createdSession.Token))
	if err != nil {
		event := events.SEP_58
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to encrypt session token: %v", err)
	}

	return base64.ParseFromBytes(encryptedSessionToken), nil
}

func (uc *SessionUseCase) getOrganizationRootPrivateKey() (string, error) {
	keyPairList, err := uc.BkConfigRepository.ListKeyPairs()
	if err != nil {
		event := events.SEP_35
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", fmt.Errorf("failed to fetch organization root key pairs list: %v", err)
	}
	if len(*keyPairList) == 0 {
		event := events.SEP_36
		uc.Logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		return "", ErrKeyPairDoesNotExist
	}

	// current implementation: we always pick the first one if multiple keys exist
	keyPair := (*keyPairList)[0]

	return keyPair.PrivateKey, nil
}

func (uc *SessionUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		uc.SessionServiceRepository,
		uc.BkConfigRepository,
	}
}
