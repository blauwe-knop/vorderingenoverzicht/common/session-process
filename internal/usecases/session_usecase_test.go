package usecases_test

import (
	"crypto/elliptic"
	"encoding/json"
	"fmt"
	"reflect"
	"testing"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/stretchr/testify/assert"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/aes"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ec"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecdsa"
	"gitlab.com/blauwe-knop/connect/go-connect/crypto/ecies"
	"gitlab.com/blauwe-knop/connect/go-connect/encoding/base64"
	bkConfigServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/model"
	bkConfigRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/bk-config-service/pkg/repositories"
	schemeProcessModel "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/model"
	schemeProcessRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/scheme-process/pkg/repositories"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest/observer"

	sessionServiceModel "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
	sessionServiceRepositories "gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/repositories"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/internal/usecases"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/pkg/model"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-process/test/mock"
)

func TestSessionUseCase_RequestChallenge(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		SessionServiceRepository sessionServiceRepositories.SessionRepository
		BkConfigRepository       bkConfigRepositories.BkConfigRepository
	}

	organizationRootKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	type Want struct {
		Nonce                       string
		EcOrganizationRootPublicKey ec.PublicKey
		Verified                    bool
	}

	tests := []struct {
		name    string
		fields  fields
		want    Want
		wantErr bool
	}{
		{
			name: "creating challenge results in a challenge",
			fields: fields{
				SessionServiceRepository: func() sessionServiceRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					ephemeralOrganizationKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
					assert.NoError(t, err)

					ephemeralOrganizationKeyPairPem, err := ephemeralOrganizationKeyPair.ToPem()
					assert.NoError(t, err)

					nonce := &sessionServiceModel.Nonce{
						Nonce:                        "nonce",
						EphemeralOrganizationKeyPair: string(ephemeralOrganizationKeyPairPem),
						CreatedAt:                    sessionServiceModel.JSONTime(time.Now()),
					}

					sessionRepository.
						EXPECT().
						CreateNonce(gomock.Any()).
						Return(nonce, nil)

					return sessionRepository
				}(),
				BkConfigRepository: func() bkConfigRepositories.BkConfigRepository {
					controller := gomock.NewController(t)

					bkConfigRepository := mock.NewMockBkConfigRepository(controller)

					organizationRootKeyPairPem, secondaryErr := organizationRootKeyPair.ToPem()
					assert.NoError(t, secondaryErr)

					organizationRootPublicKeyPem, secondaryErr := organizationRootKeyPair.PublicKey.ToPem()
					assert.NoError(t, secondaryErr)

					bkConfigRepository.
						EXPECT().
						ListKeyPairs().
						Return(
							&[]bkConfigServiceModel.KeyPair{
								{
									Id:         "1",
									PrivateKey: string(organizationRootKeyPairPem),
									PublicKey:  string(organizationRootPublicKeyPem),
								},
							},
							nil,
						)

					return bkConfigRepository
				}(),
			},
			want: Want{
				Nonce:                       "nonce",
				EcOrganizationRootPublicKey: organizationRootKeyPair.PublicKey,
				Verified:                    true,
			},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &usecases.SessionUseCase{
				Logger:                   observedLogger,
				SessionServiceRepository: tt.fields.SessionServiceRepository,
				BkConfigRepository:       tt.fields.BkConfigRepository,
				SchemeProcessRepository:  nil,
				SessionExpirationMinutes: 5,
			}
			got, err := uc.RequestChallenge()
			if (err != nil) != tt.wantErr {
				t.Errorf("RequestChallenge() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want.Nonce, got.Nonce, "nonce should be equal")

			ephemeralOrganizationEcPublicKey, err := ec.ParsePublicKeyFromPem([]byte(got.EphemeralOrganizationEcPublicKey))
			assert.NoError(t, err)

			assert.True(t, ephemeralOrganizationEcPublicKey.Verify(), "ephemeralOrganizationEcPublicKey should be valid")

			gotOrganizationNonceSignatureBytes, err := base64.Base64String(got.OrganizationNonceSignature).ToBytes()
			assert.NoError(t, err)

			verified := ecdsa.Verify(&tt.want.EcOrganizationRootPublicKey, []byte(got.Nonce), gotOrganizationNonceSignatureBytes)
			assert.Equal(t, tt.want.Verified, verified, "organizationNonceSignature should be valid")

		})
	}
}

func TestSessionUseCase_CompleteChallenge(t *testing.T) {
	observedZapCore, _ := observer.New(zap.InfoLevel)
	observedLogger := zap.New(observedZapCore)

	type fields struct {
		SessionServiceRepository sessionServiceRepositories.SessionRepository
		SchemeProcessRepository  schemeProcessRepositories.SchemeRepository
		SessionExpirationMinutes int
	}

	sessionAesKey, err := aes.GenerateKey(aes.AES128)
	assert.NoError(t, err)

	sessionAesKeyString := base64.ParseFromBytes(sessionAesKey)

	ephemeralOrganizationKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	appManagerKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	appManagerPublicKeyPem, err := appManagerKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	appKeyPair, err := ec.GenerateKeyPair(elliptic.P256())
	assert.NoError(t, err)

	appKeyPairPem, err := appKeyPair.PublicKey.ToPem()
	assert.NoError(t, err)

	tests := []struct {
		name    string
		fields  fields
		arg     model.ChallengeResponse
		want    string
		wantErr bool
	}{
		{
			name: "complete challenge with CertificateTypeNone results in a new session",
			fields: fields{
				SessionServiceRepository: func() sessionServiceRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					ephemeralOrganizationKeyPairPem, err := ephemeralOrganizationKeyPair.ToPem()
					assert.NoError(t, err)

					nonce := &sessionServiceModel.Nonce{
						Nonce:                        "nonce",
						EphemeralOrganizationKeyPair: string(ephemeralOrganizationKeyPairPem),
						CreatedAt:                    sessionServiceModel.JSONTime(time.Now()),
					}

					sessionRepository.
						EXPECT().
						DeleteNonce("nonce").
						Return(nonce, nil)

					session := &sessionServiceModel.Session{
						Token:        "token",
						AppPublicKey: "app-public-key",
						AesKey:       sessionAesKeyString,
						Scope:        "",
						Bsn:          "",
						CreatedAt:    sessionServiceModel.JSONTime(time.Now()),
						ExpiresAt:    sessionServiceModel.JSONTime(time.Now().Add(time.Minute * time.Duration(5))),
					}
					expectedSession := TestSession{
						appPublicKeyPem: string(appKeyPairPem),
						aesKey:          sessionAesKeyString,
					}
					sessionRepository.
						EXPECT().
						CreateSession(SessionMatcher(expectedSession)).
						Return(session, nil)

					return sessionRepository
				}(),
				SchemeProcessRepository:  nil,
				SessionExpirationMinutes: 5,
			},
			arg: func() model.ChallengeResponse {
				appNonceSignature, err := ecdsa.Sign(appKeyPair, []byte("nonce"))
				assert.NoError(t, err)

				challengeResult := model.ChallengeResult{
					AppNonceSignature: base64.ParseFromBytes(appNonceSignature),
					AppPublicKey:      string(appKeyPairPem),
					CertificateType:   "CertificateTypeNone",
					Certificate:       "",
					SessionAesKey:     sessionAesKeyString,
				}

				challengeResultAsJson, err := json.Marshal(challengeResult)
				assert.NoError(t, err)

				response, err := ecies.Encrypt(&ephemeralOrganizationKeyPair.PublicKey, challengeResultAsJson)
				assert.NoError(t, err)

				return model.ChallengeResponse{
					Nonce:                    "nonce",
					EncryptedChallengeResult: base64.ParseFromBytes(response),
				}
			}(),
			want:    "token",
			wantErr: false,
		},
		{
			name: "complete challenge with AppManagerJWTCertificate results in a new session",
			fields: fields{
				SessionServiceRepository: func() sessionServiceRepositories.SessionRepository {
					controller := gomock.NewController(t)

					sessionRepository := mock.NewMockSessionRepository(controller)

					ephemeralOrganizationKeyPairPem, err := ephemeralOrganizationKeyPair.ToPem()
					assert.NoError(t, err)

					nonce := &sessionServiceModel.Nonce{
						Nonce:                        "nonce",
						EphemeralOrganizationKeyPair: string(ephemeralOrganizationKeyPairPem),
						CreatedAt:                    sessionServiceModel.JSONTime(time.Now()),
					}

					sessionRepository.
						EXPECT().
						DeleteNonce("nonce").
						Return(nonce, nil)

					session := &sessionServiceModel.Session{
						Token:        "token",
						AppPublicKey: string(appKeyPairPem),
						AesKey:       sessionAesKeyString,
						Scope:        "",
						Bsn:          "",
						CreatedAt:    sessionServiceModel.JSONTime(time.Now()),
						ExpiresAt:    sessionServiceModel.JSONTime(time.Now().Add(time.Minute * time.Duration(5))),
					}
					expectedSession := TestSession{
						appPublicKeyPem: string(appKeyPairPem),
						aesKey:          sessionAesKeyString,
					}
					sessionRepository.
						EXPECT().
						CreateSession(SessionMatcher(expectedSession)).
						Return(session, nil)

					return sessionRepository
				}(),
				SchemeProcessRepository: func() schemeProcessRepositories.SchemeRepository {
					controller := gomock.NewController(t)

					appManager := schemeProcessModel.AppManager{
						Oin:          "12345",
						Name:         "app manager",
						DiscoveryUrl: "https://discovery.url",
						PublicKey:    string(appManagerPublicKeyPem),
					}

					schemeRepository := mock.NewMockSchemeRepository(controller)

					schemeRepository.
						EXPECT().
						FetchAppManager(appManager.Oin).
						Return(
							&appManager,
							nil,
						)

					return schemeRepository
				}(),
				SessionExpirationMinutes: 5,
			},
			arg: func() model.ChallengeResponse {

				appPublicKeyPem, err := appKeyPair.PublicKey.ToPem()
				assert.NoError(t, err)

				appNonceSignature, err := ecdsa.Sign(appKeyPair, []byte("nonce"))
				assert.NoError(t, err)

				appManager := schemeProcessModel.AppManager{
					Oin:          "12345",
					Name:         "app manager",
					DiscoveryUrl: "https://discovery.url",
					PublicKey:    string(appManagerPublicKeyPem),
				}

				now := time.Now().UTC()

				leewayNotBeforeMinutes := -2

				certificateExpirationMinutes := 5

				bsn := "123456789"

				claims := make(jwt.MapClaims)
				claims["app_public_key"] = string(appPublicKeyPem)
				claims["app_manager_oin"] = appManager.Oin
				claims["app_manager_public_key"] = string(appManagerPublicKeyPem)
				claims["given_name"] = "John"
				claims["family_name"] = "Doe"
				claims["scope"] = "nl.vorijk.oauth_scope.blauwe_knop"
				claims["bsn"] = bsn
				claims["iat"] = now.Unix()
				claims["nbf"] = now.Add(time.Minute * time.Duration(leewayNotBeforeMinutes)).Unix()
				claims["exp"] = now.Add(time.Minute * time.Duration(certificateExpirationMinutes)).Unix()

				accessToken, err := jwt.NewWithClaims(jwt.SigningMethodES256, claims).SignedString(appManagerKeyPair.ToEcdsa())
				assert.NoError(t, err)

				challengeResult := model.ChallengeResult{
					AppNonceSignature: base64.ParseFromBytes(appNonceSignature),
					AppPublicKey:      string(appPublicKeyPem),
					CertificateType:   "AppManagerJWTCertificate",
					Certificate:       accessToken,
					SessionAesKey:     sessionAesKeyString,
				}

				challengeResultAsJson, err := json.Marshal(challengeResult)
				assert.NoError(t, err)

				response, err := ecies.Encrypt(&ephemeralOrganizationKeyPair.PublicKey, challengeResultAsJson)
				assert.NoError(t, err)

				return model.ChallengeResponse{
					Nonce:                    "nonce",
					EncryptedChallengeResult: base64.ParseFromBytes(response),
				}
			}(),
			want:    "token",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &usecases.SessionUseCase{
				Logger:                   observedLogger,
				SessionServiceRepository: tt.fields.SessionServiceRepository,
				BkConfigRepository:       nil,
				SchemeProcessRepository:  tt.fields.SchemeProcessRepository,
				SessionExpirationMinutes: tt.fields.SessionExpirationMinutes,
			}
			gotEncryptedSessionToken, err := uc.CompleteChallenge(tt.arg)
			if (err != nil) != tt.wantErr {
				t.Errorf("CompleteChallenge() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			gotEncryptedSessionTokenBytes, err := base64.Base64String(gotEncryptedSessionToken).ToBytes()
			assert.NoError(t, err)

			gotDecryptedSessionToken, err := aes.Decrypt(sessionAesKey, gotEncryptedSessionTokenBytes)
			assert.NoError(t, err)

			assert.Equal(t, string(gotDecryptedSessionToken), tt.want)

		})
	}
}

type TestSession struct {
	appPublicKeyPem string
	aesKey          base64.Base64String
}

func SessionMatcher(input interface{}) gomock.Matcher {
	session := input.(TestSession)
	t := reflect.TypeOf(input)
	if t.Kind() != reflect.Struct {
		panic("argument to TestSessionMatcher must be a struct")
	}

	return TestSession{session.appPublicKeyPem, session.aesKey}
}

func (s TestSession) Matches(x interface{}) bool {
	gotSession := x.(sessionServiceModel.Session)
	if gotSession.Token == "" {
		fmt.Println("Token should not be empty")
		return false
	}
	if s.appPublicKeyPem != gotSession.AppPublicKey {
		fmt.Println("AppPublicKeyPem should be equal to expectedSession.appPublicKeyPem")
		return false
	}
	if s.aesKey != gotSession.AesKey {
		fmt.Println("AesKey should be equal to expectedSession.aesKey")
		return false
	}
	gotSessionCreatedAt := (time.Time)(gotSession.CreatedAt)
	gotSessionExpiresAt := (time.Time)(gotSession.ExpiresAt)
	if gotSessionExpiresAt.Before(gotSessionCreatedAt) {
		fmt.Println("session expires at should be after session created at")
		return false
	}
	return true
}

func (s TestSession) String() string {
	return "TestSession"
}
